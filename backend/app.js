const express = require("express");
const app = express();
const errorMiddleware = require('./middlewares/error')

// Middleware setup and route definitions
// ...

app.use(express.json());

const products = require("./routes/product");

app.use("/api/v1/", products);

app.use(errorMiddleware)

module.exports = app; // Export the Express app instance
